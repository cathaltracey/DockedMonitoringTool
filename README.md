# Docker Monitoring Tool

Author: Cathal Tracey

Instructions
1 - Ensure Docker is installed on your machine


2 - Open Docker Machine and run sudo docker -d -H unix:///var/run/docker.sock -H 0.0.0.0:4243 &. 

   (This will open a port at 4243 so the application can communicate with Docker)
                                                                                                
3 - npm install

4 - bower install

5 - type 'node server' into terminal

6 - That's it.

